/**********************************************************************************************
*
*   Last author : Quentin RAYMONDAUD 
*
**********************************************************************************************/


#include "raylib.h"
#include "screens.h"
#include "raymath.h"

#ifndef	_CAMERA_H
#define	_CAMERA_H

#ifndef _PLAYER_H
#include "player.h"
#endif

static Camera2D camera = { 0 };
static int cameraOption = 0;
static int cameraOption;
static int cameraUpdatersLength;
static char *cameraDescriptions[] = {
    "Follow player center",
    "Follow player center, but clamp to map edges",
    "Follow player center; smoothed",
    "Follow player center horizontally; updateplayer center vertically after landing",
    "Player push camera on getting too close to screen edge"
};


// Store pointers to the multiple update camera functions
void UpdateCameraCenter(Camera2D *camera, Player *player, EnvItem *envItems, int envItemsLength, float delta, int width, int height);


void (*cameraUpdaters[])(Camera2D*, Player*, EnvItem*, int, float, int, int) = {
    UpdateCameraCenter,
};

void UpdateCameraCenter(Camera2D *camera, Player *player, EnvItem *envItems, int envItemsLength, float delta, int width, int height){
    camera->offset = (Vector2){ width/2.0f, height/2.0f };
    camera->target = player->position;
}

#endif