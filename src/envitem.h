/**********************************************************************************************
*
*   Last author : Quentin RAYMONDAUD 
*
**********************************************************************************************/

#ifndef _ENVITEM_H
#define _ENVITEM_H

#define     MAX_PLATEFORMS      14

typedef struct EnvItem {
    Rectangle rect;
    Vector2 speed;
    Color color;
} EnvItem;

typedef struct PlateformManager{
    struct EnvItem plateforms[MAX_PLATEFORMS]; 
    int currentIndex;
} PlateformManager;

PlateformManager plateformManager;

// Adds or replace non static EnvItem
bool add(PlateformManager *manager, EnvItem added ){
    bool managerExists = (manager != (void*)0);
    if ( managerExists ){
        int nextIndex = (manager->currentIndex+1) % MAX_PLATEFORMS;
        while ( manager->plateforms[nextIndex].speed.x +
                manager->plateforms[nextIndex].speed.y != 0 ) nextIndex += 1;   
        manager->plateforms[nextIndex] = added;
        manager->currentIndex = nextIndex;
    }
    return managerExists;
}

bool replace(PlateformManager *manager, EnvItem added, int index ){
    bool replaceOk = ( index < MAX_PLATEFORMS && manager != (void*)0 );
    if ( replaceOk ) manager->plateforms[index] = added;
    return replaceOk;
}

// Updates non static EnvItems
bool updatePlateforms(PlateformManager *plateformManager, float deltaTime){
    bool lost = false; 
    EnvItem * current = (void*)0;
    for ( int i = 0; i < MAX_PLATEFORMS; i++){
        current = &(plateformManager->plateforms[i]);
        if ( current->speed.x + current->speed.y != 0 ){
            current->rect.x += current->speed.x * deltaTime;
            current->rect.y += current->speed.y * deltaTime;
            if (current->rect.x < 0 || current->rect.y > screenHeight - current->rect.height ){
                /* POP A NEW RED BLOCK */
                if ( current->color.r + current->color.g + current->color.b == 0){
                    float newWidth = current->rect.width + GetRandomValue(0,1) * GetRandomValue(-current->rect.width/2,current->rect.width/2);
                    replace(
                        plateformManager,
                        (EnvItem){
                            {   screenWidth*2 - newWidth,
                                screenHeight - ((MAX_PLATEFORMS-4 + i) * GetRandomValue(15,45)),
                                newWidth,
                                30
                            },
                            (Vector2){
                                current->speed.x - GetRandomValue(-10,10),
                                current->speed.y + GetRandomValue(0,1) * GetRandomValue(0,20)
                            },
                            RED
                        }
                    ,i);
                /* IT'S LOST BECAUSE RED TOUCHED BLACK */
                } else { 
                    lost = true;
                    break;
                }
            } 
        }
    }
    return lost;
}
#endif