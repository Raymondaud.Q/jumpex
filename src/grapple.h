#ifndef _GRAPPLE_H
#define _GRAPPLE_H
#define     GRAPPLE_SPD             1000
#define     GRAPPLE_MAX_LENGTH      400
#define     GRAPPLE_SIZE            20
#define     ARROW_SIZE              (GRAPPLE_SIZE/2)/tanf(20*DEG2RAD)

typedef enum {
    ST_REWINDING = -1,
    ST_IDLING,
    ST_THROWING,
    ST_STUCK,
} GrappleState;

typedef struct Grapple {
    Vector2 position;
    Vector2 triangle[3];
    Vector2 speed;
    Color color;
    float angle;
    int score;
    Timer soundTimer;
    int state; // -1 = rewinding, 0 = idle, 1 = throwing, 2 = stuck, 3 = grapplePlayer
} Grapple;

void checkCollision(Grapple *grp, EnvItem *envItems, int envItemsLength){
    for (int i = 0; i < envItemsLength; i++){
        EnvItem *ei = envItems + i;
        if ( CheckCollisionPointRec( grp->position, ei->rect )  ){
            grp->speed.x = ei->speed.x;
            grp->speed.y = ei->speed.y;
            grp->state = ST_STUCK;
            if ( ei->color.r == 230 && ei->color.g == 41 && ei->color.b == 55){
                ei->color = BLACK;
                grp->color = RED;
                grp->score += 1;
                PlaySound(soundEffects[POINT]);
            } else {
                if ( TimerDone(&grp->soundTimer) ){
                    PlaySound(soundEffects[STUCK]);
                    StartTimer(&grp->soundTimer, 0.75);
                } ;
            }
            break;
        } else {
            if (grp->state == ST_STUCK){
                grp->state = ST_REWINDING;
                PlaySound(soundEffects[REWIND]);
            }
        }
    }
}

void updateTriangle(Grapple *grp){
    grp->triangle[0] = (Vector2){ grp->position.x + sinf(grp->angle * DEG2RAD)*(ARROW_SIZE), grp->position.y - cosf(grp->angle * DEG2RAD)*(ARROW_SIZE) };
    grp->triangle[1] = (Vector2){ grp->position.x - cosf(grp->angle * DEG2RAD)*(GRAPPLE_SIZE/2), grp->position.y - sinf(grp->angle * DEG2RAD)*(GRAPPLE_SIZE/2) };
    grp->triangle[2] = (Vector2){ grp->position.x + cosf(grp->angle * DEG2RAD)*(GRAPPLE_SIZE/2), grp->position.y + sinf(grp->angle * DEG2RAD)*(GRAPPLE_SIZE/2) };
}

int updateGrapple(Grapple *grp, Vector2 playerPos, EnvItem *envItems, int envItemsLength, float deltaTime, Camera2D camera){

    Vector2 mousePosition = GetMousePosition();
    Vector2 mouseInWorld = GetScreenToWorld2D(mousePosition, camera);
    if ( grp->state == ST_IDLING ){
        grp->angle = atan2f(mouseInWorld.y -  grp->position.y, mouseInWorld.x -  grp->position.x) + PI/2;
        grp->position.x = playerPos.x;
        grp->position.y = playerPos.y;
        grp->color = BLACK;
        updateTriangle(grp);
    } else {
        grp->position.x += grp->speed.x * deltaTime;
        grp->position.y += grp->speed.y * deltaTime;
        updateTriangle(grp);
    }
    if (grp->state == ST_STUCK) checkCollision(grp, envItems, envItemsLength);
    else if ( grp->state == ST_THROWING || grp->state == ST_REWINDING ) {
        float distGP = Vector2Distance(grp->position, playerPos);
        if (grp->state == ST_THROWING && distGP >= GRAPPLE_MAX_LENGTH){
            grp->state = ST_REWINDING; 
            PlaySound(soundEffects[REWIND]);
        }
        else if (grp->state == ST_REWINDING){
            grp->angle = atan2f(playerPos.y -  grp->position.y, playerPos.x -  grp->position.x) + PI/2;
            if ( distGP < 20 && distGP > -20) grp->state = ST_IDLING;
            grp->speed.x = cosf(grp->angle - PI/2) * GRAPPLE_SPD ;
            grp->speed.y = sinf(grp->angle - PI/2) * GRAPPLE_SPD ;
        } else {
            grp->speed.x = cosf(grp->angle - PI/2) * GRAPPLE_SPD ;
            grp->speed.y = sinf(grp->angle - PI/2) * GRAPPLE_SPD ;
            checkCollision(grp, envItems, envItemsLength);
        } 
    } 
    return grp->state;
}


bool throwGrapple(Grapple *grp){
    bool isIdling = (grp->state == ST_IDLING);
    bool isThrewOrStuck = ( grp->state == ST_THROWING || grp->state == ST_STUCK) ;
    if (isIdling) grp->state=ST_THROWING;
    else if (isThrewOrStuck) grp->state=ST_REWINDING;
    return (isIdling || isThrewOrStuck);
}

#endif