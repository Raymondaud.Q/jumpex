#ifndef _PLAYER_H
#define _PLAYER_H
#define     G                       500
#define     PLAYER_SIZE             40
#define     PLAYER_JUMP_SPD         230.0f
#define     PLAYER_JUMP_INTERVAL    0.50f
#define     ENERGY_MAX_RADIUS       150.0f
#define     ENERGY_MIN_RADIUS       15.0f
#define     ENERGY_INC              35.0f
#define     GRIP_RATE               200.0f
#include "envitem.h"
#include "grapple.h"
#ifndef _CAMERA_H
    #include "camera.h"
#endif

typedef struct Player {
    Vector2 position;
    Vector2 speed;
    float angle;
    float boost;
    bool grounded;
    Grapple grapple;
    Timer jumpTimer;
} Player;

void UpdatePlayer(Player *player, EnvItem *envItems, int envItemsLength, float delta,  Camera2D camera);

Player player;
Rectangle playerRect;
// Get variable (uniform) location on the shader to connect with the program
// NOTE: If uniform variable could not be found in the shader, function returns -1
int swirlCenterValue;
// Value to set to the previous var
Vector2 swirlCenter;
// Get variable (uniform) location on the shader to connect with the program
int swirlRadiusValue;
// Value to set to the previous var
float swirlRadius;
// Get variable (uniform) location on the shader to connect with the program
int swirlAngleValue;
// Value to set to the previous var
float swirlAngle;

float fogRadius;
int fogRadiusValue;
Vector2 fogCenter;
int fogCenterValue;
Vector2 fogTarget;
int fogTargetValue;

bool updatePlayer(Player *player, EnvItem *envItems, int envItemsLength, float delta, Camera2D camera){
    
    Vector2 mousePosition = GetMousePosition();
    Vector2 mouseInWorld = GetScreenToWorld2D(mousePosition, camera);
    swirlCenter.x = mousePosition.x;
    swirlCenter.y = screenHeight - mousePosition.y;
    swirlAngle = player->angle;

    Vector2 mouseInScreen = GetWorldToScreen2D(mousePosition, camera);
    fogTarget.x = mouseInScreen.x;
    fogTarget.y = screenHeight - mouseInScreen.y;

    Vector2 playerInScreen = GetWorldToScreen2D(player->position, camera);
    fogCenter.x = playerInScreen.x;
    fogCenter.y = playerInScreen.y;

    fogRadius = 650.0f - swirlRadius*2;
    player->angle = atan2f(mouseInWorld.y - player->position.y, mouseInWorld.x - player->position.x);
    swirlAngle = player->angle - PI/2;

    bool collideWithEnergy = CheckCollisionCircleRec(mouseInWorld, swirlRadius, playerRect);
    if ( collideWithEnergy ){
        if ( swirlRadius < ENERGY_MAX_RADIUS ) swirlRadius += ENERGY_INC * delta;  
        if ( TimerDone(&player->jumpTimer) ){
            player->speed.y = sinf(player->angle) * (PLAYER_JUMP_SPD + swirlRadius+player->boost);
            player->speed.x = cosf(player->angle) * (PLAYER_JUMP_SPD + swirlRadius+player->boost);
            PlaySound(jumpEffects[GetRandomValue(0,5)]);
            StartTimer(&player->jumpTimer, PLAYER_JUMP_INTERVAL);
        } 
    } else if (swirlRadius > ENERGY_MIN_RADIUS ) swirlRadius -=  ENERGY_INC * delta * 2;

    int hitEnvItems = 0;
    bool go = false;
    for (int i = 0; i < envItemsLength; i++){
        EnvItem *ei = envItems + i;
        Vector2 *p = &(player->position);
        if (ei->color.r + ei->color.g + ei->color.b == 0 ){
            if ( CheckCollisionRecs(playerRect, ei->rect)){
                go = true;
                break;
            }
        } else if (ei->rect.x <= p->x &&
            ei->rect.x + ei->rect.width >= p->x &&
            ei->rect.y >= p->y &&
            ei->rect.y < p->y + player->speed.y*delta){
            hitEnvItems = 1;
            player->position.y += ei->speed.y*delta;
            if (ei->speed.x != player->speed.x && ! collideWithEnergy){
                if (player->speed.x+10 < ei->speed.x ){
                    player->speed.x += (ei->speed.x > 0 ? 1 : -1 ) * ei->speed.x/10;
                } else if (player->speed.x-10 > ei->speed.x ){
                    player->speed.x -=  (ei->speed.x > 0 ? 1 : -1 ) * ei->speed.x/10;
                } else {
                     player->speed.x = ei->speed.x;
                }
            }
            break;
        }
    }
    player->grounded = hitEnvItems;
    if (!hitEnvItems ){
        player->position.y += player->speed.y*delta;
        player->speed.y += G*delta;
    }
    player->position.x += player->speed.x * delta;
    player->speed.x += (player->speed.x > 0 ? -delta : delta ) * GRIP_RATE;
    return go;
}

#endif
