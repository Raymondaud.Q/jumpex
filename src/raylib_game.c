/**********************************************************************************************
*
*   Last author : Quentin RAYMONDAUD 
*
**********************************************************************************************/
/*******************************************************************************************
*
*   raylib game template
*
*   <Game title>
*   <Game description>
*
*   This game has been created using raylib (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2021 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
#include "raylib.h"
#include "screens.h" // NOTE: Declares global (extern) variables and screens functions
#include <stdlib.h>
#include <time.h>

#if defined(PLATFORM_WEB)
    #include <emscripten/emscripten.h>
#endif
//----------------------------------------------------------------------------------
// Shared Variables Definition (global)
// NOTE: Those variables are shared between modules through screens.h
//----------------------------------------------------------------------------------
GameScreen currentScreen = 0;
int bestScore = 0;
Sound soundEffects[6];
Sound jumpEffects[6];
Music music = { 0 };
Font font = { 0 };

int screenWidth = 800;
int screenHeight = 450; 
double GetElapsed(Timer* timer){ return GetTime() - timer->StartTime;}
bool TimerDone(Timer* timer){ return GetTime() - timer->StartTime >= timer->Lifetime;}
void StartTimer(Timer* timer, double lifetime){ timer->StartTime = GetTime();timer->Lifetime = lifetime;}
//----------------------------------------------------------------------------------
// Local Variables Definition (local to this module)
//----------------------------------------------------------------------------------
// Required variables to manage screen transitions (fade-in, fade-out)
static float transAlpha = 0.0f;
static bool onTransition = false;
static bool transFadeOut = false;
static int transFromScreen = -1;
static int transToScreen = -1;
//----------------------------------------------------------------------------------
// Local Functions Declaration
//----------------------------------------------------------------------------------
static void ChangeToScreen(int screen);     // Change to screen, no transition effect
static void TransitionToScreen(int screen); // Request transition to next screen
static void UpdateTransition(void);         // Update transition effect
static void DrawTransition(void);           // Draw transition effect (full-screen rectangle)
static void UpdateDrawFrame(void);          // Update and draw one frame
//----------------------------------------------------------------------------------
// Main entry point
//----------------------------------------------------------------------------------
int main(void){
    InitWindow(screenWidth, screenHeight, "raylib game template");
    InitAudioDevice();      // Initialize audio device
    // Load global data (assets that must be available in all screens, i.e. font)
    font = LoadFont("resources/mecha.png");
    bestScore = 0;
    soundEffects[DIE] = LoadSound("resources/dead.wav");
    soundEffects[SHOT] = LoadSound("resources/shot.wav");
    soundEffects[REWIND] = LoadSound("resources/rewind.wav");
    soundEffects[STUCK] = LoadSound("resources/stuck.wav");
    soundEffects[POINT] = LoadSound("resources/inc.wav");
    soundEffects[CLICK] = LoadSound("resources/click.wav");
    for ( int i = 0; i < 6; i++){
        jumpEffects[i] = LoadSound(TextFormat("resources/jump_%i.wav",i));
        SetSoundVolume(jumpEffects[i], 0.3f);
        SetSoundVolume(soundEffects[i], 0.3f);
    }
    music = LoadMusicStream("resources/8-Subversion-FootPrintSystem.mp3");
    /// Setup and init first screen
    currentScreen = LOGO;
    InitLogoScreen();
#if defined(PLATFORM_WEB)
    emscripten_set_main_loop(UpdateDrawFrame, 60, 1);
#else
    SetTargetFPS(60);       // Set our game to run at 60 frames-per-second
    while (!WindowShouldClose()) UpdateDrawFrame();
#endif
    switch (currentScreen){
        case LOGO: UnloadLogoScreen(); break;
        case TITLE: UnloadTitleScreen(); break;
        case GAMEPLAY: UnloadGameplayScreen(); break;
        default: break;
    }
    UnloadFont(font);
    //UnloadMusicStream(music);
    for (int i = 0; i < 7; i++) UnloadSound(soundEffects[i]);
    CloseAudioDevice();     // Close audio context
    CloseWindow();          // Close window and OpenGL context
    return 0;
}
//----------------------------------------------------------------------------------
// Module specific Functions Definition
//----------------------------------------------------------------------------------
// Change to next screen, no transition
static void ChangeToScreen(int screen){
    // Unload current screen
    switch (currentScreen) {
        case LOGO: UnloadLogoScreen(); break;
        case TITLE: UnloadTitleScreen(); break;
        case GAMEPLAY: UnloadGameplayScreen(); break;
        default: break;
    } switch (screen) {
        case LOGO: InitLogoScreen(); break;
        case TITLE: InitTitleScreen(); break;
        case GAMEPLAY: InitGameplayScreen(); break;
        default: break;
    }
    currentScreen = screen;
}
// Request transition to next screen
static void TransitionToScreen(int screen){
    onTransition = true;
    transFadeOut = false;
    transFromScreen = currentScreen;
    transToScreen = screen;
    transAlpha = 0.0f;
}
// Update transition effect (fade-in, fade-out)
static void UpdateTransition(void){
    if (!transFadeOut){
        transAlpha += 0.05f;
        // NOTE: Due to float internal representation, condition jumps on 1.0f instead of 1.05f
        // For that reason we compare against 1.01f, to avoid last frame loading stop
        if (transAlpha > 1.01f){
            transAlpha = 1.0f;
            // Unload current screen
            switch (transFromScreen){
                case LOGO: UnloadLogoScreen(); break;
                case TITLE: UnloadTitleScreen(); break;
                case OPTIONS: UnloadOptionsScreen(); break;
                case GAMEPLAY: UnloadGameplayScreen(); break;
                default: break;
            }
            // Load next screen
            switch (transToScreen){
                case LOGO: InitLogoScreen(); break;
                case TITLE: InitTitleScreen(); break;
                case GAMEPLAY: InitGameplayScreen(); break;
                case OPTIONS: InitOptionsScreen(); break;
                default: break;
            }
            currentScreen = transToScreen;
            // Activate fade out effect to next loaded screen
            transFadeOut = true;
        }
    } else { // Transition fade out logic
        transAlpha -= 0.02f;
        if (transAlpha < -0.01f){
            transAlpha = 0.0f;
            transFadeOut = false;
            onTransition = false;
            transFromScreen = -1;
            transToScreen = -1;
        }
    }
}
static void DrawTransition(void){ DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), Fade(BLACK, transAlpha)); }
static void UpdateDrawFrame(void){
    UpdateMusicStream(music); // NOTE: Music keeps playing between screens
    if (!onTransition){
        switch(currentScreen){
            case LOGO:{
                UpdateLogoScreen();
                if (FinishLogoScreen()) TransitionToScreen(TITLE);
            } break;
            case TITLE:{
                UpdateTitleScreen();
                if (FinishTitleScreen() == 1) TransitionToScreen(OPTIONS);
                else if (FinishTitleScreen() == 2) TransitionToScreen(GAMEPLAY);
            } break;
            case OPTIONS:{
                UpdateOptionsScreen();
                if (FinishOptionsScreen()) TransitionToScreen(TITLE);
            } break;
            case GAMEPLAY:{
                UpdateGameplayScreen();
                if (FinishGameplayScreen() == 1) TransitionToScreen(TITLE);
            } break;
        }
    }
    else UpdateTransition();    // Update transition (fade-in, fade-out)
    BeginDrawing();
        switch(currentScreen){
            case LOGO: DrawLogoScreen(); break;
            case TITLE: DrawTitleScreen(); break;
            case OPTIONS: DrawOptionsScreen(); break;
            case GAMEPLAY: DrawGameplayScreen(); break;
            default: break;
        } if (onTransition) DrawTransition();
    EndDrawing();
}
