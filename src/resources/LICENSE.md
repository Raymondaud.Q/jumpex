# Credits :

* **8-Subversion-FootPrintSystem.mp3** - [FootPrint System](https://odgprod.com/release/footprint-system-shakti/)
	* FootPrint System's authorization for Quentin RAYMONDAUD's non-commercial project
* **Effects** - [Made with rRF](https://raylibtech.itch.io/rfxgen)
* **Shaders** - Adapted from [raylib shader example](https://github.com/raysan5/raylib/tree/master/examples/shaders/resources/shaders)
* **Mecha.png Font** - Taken from [raylib game template](https://github.com/raysan5/raylib-game-template/tree/main/src/resources)