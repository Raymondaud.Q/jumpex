#version 120

// Input vertex attributes (from vertex shader)
varying vec2 fragTexCoord;
// Input uniform values
uniform sampler2D texture0;

void main(){
    vec3 tc = texture2D(texture0, fragTexCoord).rgb;
    gl_FragColor = vec4(tc, 1.0);
}