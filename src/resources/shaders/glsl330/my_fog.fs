#version 330

// Input vertex attributes (from vertex shader)
in vec2 fragTexCoord;
in vec4 fragColor;
in vec3 fragPosition;
in vec3 fragNormal;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;

uniform vec2 center;
uniform vec2 target;
uniform float radius;
uniform float angle;

// Output fragment color
out vec4 finalColor;

// NOTE: Render size values should be passed from code
const float renderWidth = 800.0;
const float renderHeight = 450.0;

void main()
{
    vec2 texSize = vec2(renderWidth, renderHeight);
    vec2 tc = fragTexCoord*texSize;
    tc -= center;
    vec2 v = normalize(target - tc); 
    /*
        float theta = dot(v, normalize(fragTexCoord - tc));
        float f = angle < theta ? 1.0 : 0.0;
    */
    float d = length( tc );
    float fock = 1.0 - ( d / radius );

    tc += center;
    vec4 color = texture2D(texture0, tc/texSize)*colDiffuse*fock*fragColor;
    finalColor = vec4(color.rgb, 1.0);
}
