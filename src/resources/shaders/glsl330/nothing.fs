#version 330

// Input vertex attributes (from vertex shader)
in vec2 fragTexCoord;

// Input uniform values
uniform sampler2D texture0;

// Output fragment color
out vec4 finalColor;

void main(){
    vec3 tc = texture(texture0, fragTexCoord).rgb;
    finalColor = vec4(tc, 1.0);
}