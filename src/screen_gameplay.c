/**********************************************************************************************
*
*   Last author : Quentin RAYMONDAUD 
*
**********************************************************************************************/

/**********************************************************************************************
*
*   raylib - Advance Game template
*
*   Gameplay Screen Functions Definitions (Init, Update, Draw, Unload)
*
*   Copyright (c) 2014-2022 Ramon Santamaria (@raysan5)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
**********************************************************************************************/

#include "raylib.h"
#include "rlgl.h"
#include "raymath.h"
#include "camera.h"
#include "player.h"
#include "screens.h"

#define     MAX_POSTPRO_SHADERS     12
#if defined(PLATFORM_DESKTOP)
    #define GLSL_VERSION            330
#else   // PLATFORM_RPI, PLATFORM_ANDROID, PLATFORM_WEB
    #define GLSL_VERSION            100
#endif

typedef enum {
    FX_NONE = 0,
    FX_POSTERIZATION,
    FX_DREAM_VISION,
    FX_PIXELIZER,
    FX_PREDATOR_VIEW,
    FX_SCANLINES,
    FX_SOBEL,
    FX_BLOOM,
    FX_BLUR,
    //FX_FXAA
} PostproShader;

//----------------------------------------------------------------------------------
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen;


//--------- Swirl Shader ----------------

// Render texture for the energy shader
static Shader swirlShader;
static RenderTexture2D swirlTexture;


// Render texture for fog
static Shader fogShader;
static RenderTexture2D fogTexture;

//--------- Effect Shader ------------
// Render texture for effect shaders
static Shader effects[MAX_POSTPRO_SHADERS] = { 0 };
static RenderTexture2D effectTexture;
static int currentShader;
static bool playerGrounded;

//----------------------------------------------------------------------------------
// Gameplay Screen Functions Definition
//----------------------------------------------------------------------------------

// Gameplay Screen Initialization logic
void InitGameplayScreen(void){
    // TODO: Initialize GAMEPLAY screen variables here!
    framesCounter = 0;
    finishScreen = 0;
    playerGrounded = false;    
    SetMusicVolume(music, 0.6f);
    PlayMusicStream(music);

    // Framebuffers
    swirlTexture = LoadRenderTexture(screenWidth, screenHeight);
    fogTexture = LoadRenderTexture(screenWidth, screenHeight);
    effectTexture = LoadRenderTexture(screenWidth, screenHeight);
  
    // Loads Postprocessing Swirl Shader
    swirlShader = LoadShader(0, TextFormat("resources/shaders/glsl%i/swirl.fs", GLSL_VERSION));
    swirlCenterValue = GetShaderLocation(swirlShader, "center");
    swirlRadiusValue = GetShaderLocation(swirlShader, "radius");
    swirlAngleValue = GetShaderLocation(swirlShader, "angle");

    // Fog Shader
    fogShader = LoadShader(0, TextFormat("resources/shaders/glsl%i/my_fog.fs", GLSL_VERSION));
    fogRadiusValue = GetShaderLocation(fogShader, "radius");
    fogCenterValue = GetShaderLocation(fogShader, "center");
    fogTargetValue = GetShaderLocation(fogShader, "target");
    //fogAngleValue = GetShaderLocation(fogShader, "angle");

    // Load all effect shaders
    effects[FX_NONE] = LoadShader(0, TextFormat("resources/shaders/glsl%i/nothing.fs", GLSL_VERSION));
    effects[FX_POSTERIZATION] = LoadShader(0, TextFormat("resources/shaders/glsl%i/posterization.fs", GLSL_VERSION));
    effects[FX_DREAM_VISION] = LoadShader(0, TextFormat("resources/shaders/glsl%i/dream_vision.fs", GLSL_VERSION));
    effects[FX_PIXELIZER] = LoadShader(0, TextFormat("resources/shaders/glsl%i/pixelizer.fs", GLSL_VERSION));
    effects[FX_PREDATOR_VIEW] = LoadShader(0, TextFormat("resources/shaders/glsl%i/predator.fs", GLSL_VERSION));
    effects[FX_SCANLINES] = LoadShader(0, TextFormat("resources/shaders/glsl%i/scanlines.fs", GLSL_VERSION));
    effects[FX_SOBEL] = LoadShader(0, TextFormat("resources/shaders/glsl%i/sobel.fs", GLSL_VERSION));
    effects[FX_BLOOM] = LoadShader(0, TextFormat("resources/shaders/glsl%i/bloom.fs", GLSL_VERSION));
    effects[FX_BLUR] = LoadShader(0, TextFormat("resources/shaders/glsl%i/blur.fs", GLSL_VERSION));
    currentShader = 0;//FX_PREDATOR_VIEW;//FX_GRAYSCALE;

    // Camera Init
    camera.target = player.position;
    camera.offset = (Vector2){ screenWidth/2.0f, screenHeight/2.0f };
    camera.rotation = 0.0f;
    camera.zoom = 1.0f;
    cameraOption = 0;
    cameraUpdatersLength = sizeof(cameraUpdaters)/sizeof(cameraUpdaters[0]);
    Vector2 mouseInWorld = GetScreenToWorld2D(GetMousePosition(), camera);
    swirlCenter.x = mouseInWorld.x;
    swirlCenter.y = mouseInWorld.y;
    swirlRadius = ENERGY_MIN_RADIUS;
    swirlAngle = player.angle;

    // Player init
    player = (Player){ 0 };
    player.position = (Vector2){PLAYER_SIZE*2, screenHeight/2.0f};
    player.speed = (Vector2){0, 0};
    player.angle = 0;
    player.boost = 1;
    player.jumpTimer = (Timer){};
    StartTimer(&player.jumpTimer,PLAYER_JUMP_INTERVAL);

    // Grapple Init 
    player.grapple.position = (Vector2){screenWidth/2, screenHeight/2 - ARROW_SIZE/2};
    player.grapple.speed = (Vector2){0, 0};
    player.grapple.angle = player.angle;
    player.grapple.score = 0;
    player.grapple.soundTimer = (Timer){};
    StartTimer(&player.grapple.soundTimer, 0.75);

    // MAP Inits
    plateformManager = (PlateformManager){0};
    add(&plateformManager, (EnvItem){{ 0, screenHeight, screenWidth*2, 100 }, (Vector2){0, 0}, BLACK});
    add(&plateformManager, (EnvItem){{ 0, -screenHeight*2, screenWidth*2, 100 }, (Vector2){0, 0}, BLACK});
    add(&plateformManager, (EnvItem){{ -100, -screenHeight*2, 100, screenHeight*3+100}, (Vector2){0, 0}, BLACK});
    add(&plateformManager, (EnvItem){{ screenWidth*2, -screenHeight*2, 100, screenHeight*3+100}, (Vector2){0, 0}, BLACK});
    int step = 0 ;
    for (int i = 0; i < MAX_PLATEFORMS-4; i++){
        if ( 200+200*i < screenWidth*2 ){
            EnvItem ei = (EnvItem){{ 200+200*i, screenHeight - 30*(i+1) , 200, 30 }, (Vector2){-40, 0}, RED };
            add(&plateformManager, ei);
        } else if ( screenWidth - 200*(i-step*2) > 400 ) { 
            EnvItem ei = (EnvItem){{screenWidth - 200*(i-step*2), screenHeight - (1+step)*65 , 200, 30 }, (Vector2){-30, 0}, RED };
            step += 1;
            add(&plateformManager, ei);
        }
    }
}

// Gameplay Screen Update logic
void UpdateGameplayScreen(void){
    if ( player.grounded != playerGrounded && player.grounded == true) currentShader=(currentShader + 1)%9;
    playerGrounded = player.grounded;
    // Send new value to the shader to be used on drawing
    SetShaderValue(swirlShader, swirlCenterValue, &swirlCenter, SHADER_UNIFORM_VEC2);
    SetShaderValue(swirlShader, swirlRadiusValue, &swirlRadius, SHADER_UNIFORM_FLOAT);
    SetShaderValue(swirlShader, swirlAngleValue, &swirlAngle, SHADER_UNIFORM_FLOAT);

    SetShaderValue(fogShader, fogTargetValue, &fogTarget, SHADER_UNIFORM_VEC2);
    SetShaderValue(fogShader, fogCenterValue, &fogCenter, SHADER_UNIFORM_VEC2);
    SetShaderValue(fogShader, fogRadiusValue, &fogRadius, SHADER_UNIFORM_FLOAT);
    //SetShaderValue(fogShader, fogAngleValue, &fogAngle, SHADER_UNIFORM_FLOAT);

    float deltaTime = GetFrameTime();
    bool isGameOver = ( updatePlayer(&player, plateformManager.plateforms, MAX_PLATEFORMS, deltaTime, camera) + 
                        updatePlateforms(&plateformManager, deltaTime) );
    updateGrapple(&player.grapple, player.position, plateformManager.plateforms, MAX_PLATEFORMS, deltaTime, camera);

    
    camera.zoom += ((float)GetMouseWheelMove()*0.05f);
    if (camera.zoom > 3.0f) camera.zoom = 3.0f;
    else if (camera.zoom < 0.25f) camera.zoom = 0.25f;

    if (IsKeyPressed(KEY_R)){
        camera.zoom = 1.0f;
        player.position = (Vector2){ screenWidth/2.0f, screenHeight/2.0f };
        player.grapple.state = 0;
    }
    
    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT) ){
        if ( !player.grounded && player.grapple.state == 0 ){
            throwGrapple(&player.grapple);
            PlaySoundMulti(soundEffects[SHOT]);
        } else if ( player.grounded && player.grapple.state > 0 ){
            throwGrapple(&player.grapple);
            PlaySoundMulti(soundEffects[REWIND]);
        }
    }

    // Call update camera function by its pointer
    cameraUpdaters[cameraOption](&camera, &player, plateformManager.plateforms, MAX_PLATEFORMS, deltaTime, screenWidth, screenHeight);
    if ( isGameOver ){
        finishScreen = 1;
        StopMusicStream(music);
        if ( player.grapple.score > bestScore ) bestScore = player.grapple.score;
        PlaySoundMulti(soundEffects[DIE]);
    }
}

// Gameplay Screen Draw logic
void DrawGameplayScreen(void){
    // Enable drawing to texture
    BeginTextureMode(swirlTexture);      
        ClearBackground(RAYWHITE);
        BeginMode2D(camera);
            for (int i = 0; i < MAX_PLATEFORMS; i++) DrawRectangleRec(plateformManager.plateforms[i].rect, plateformManager.plateforms[i].color);
            playerRect = (Rectangle){ player.position.x - (PLAYER_SIZE/2), player.position.y - PLAYER_SIZE, PLAYER_SIZE, PLAYER_SIZE };
            // Draw player 
            rlPushMatrix();
            rlTranslatef(player.position.x, player.position.y-PLAYER_SIZE/2, 0);
            rlRotatef(player.angle * RAD2DEG, 0, 0, 1);
            rlTranslatef(-player.position.x, -player.position.y+PLAYER_SIZE/2, 0);
            DrawRectangleRec(playerRect, RED);
            rlPopMatrix();

            // Draw Grapple
            rlPushMatrix();
            rlTranslatef(player.grapple.position.x, player.grapple.position.y-PLAYER_SIZE/2, 0);
            rlRotatef(player.grapple.angle * RAD2DEG, 0, 0, 1);
            rlTranslatef(-player.grapple.position.x, -player.grapple.position.y, 0);
            DrawTriangle(player.grapple.triangle[0],player.grapple.triangle[1],player.grapple.triangle[2], player.grapple.color);
            rlPopMatrix();
        EndMode2D(); 
    EndTextureMode();

    // Second Pass - Shadow
    BeginTextureMode(effectTexture);  
        ClearBackground(RAYWHITE);     
        BeginShaderMode(fogShader);
            DrawTextureRec(swirlTexture.texture, (Rectangle){ 0, 0, (float)swirlTexture.texture.width, (float)-swirlTexture.texture.height }, (Vector2){ 0, 0 }, WHITE);
        EndShaderMode();
    EndTextureMode();

    // Third Pass - Energy distorsion
    BeginTextureMode(fogTexture);  
        ClearBackground(RAYWHITE);     
        BeginShaderMode(swirlShader);
            DrawTextureRec(effectTexture.texture, (Rectangle){ 0, 0, (float)effectTexture.texture.width, (float)-effectTexture.texture.height }, (Vector2){ 0, 0 }, WHITE);
        EndShaderMode();
        DrawText(TextFormat("%i",player.grapple.score), 10, 10, font.baseSize*4, RED);
    EndTextureMode();

    // Fourth Pass - Effects
    BeginShaderMode(effects[currentShader]);
        DrawTextureRec(fogTexture.texture, (Rectangle){ 0, 0, (float)fogTexture.texture.width, (float)-fogTexture.texture.height }, (Vector2){ 0, 0 }, WHITE);   
    EndShaderMode();
    DrawText(TextFormat("%i fps", GetFPS()), 10, 5, font.baseSize*0.5, RED);
}

// Gameplay Screen Unload logic
void UnloadGameplayScreen(void){}

// Gameplay Screen should finish?
int FinishGameplayScreen(void){
    return finishScreen;
}