/**********************************************************************************************
*
*   raylib - Advance Game template
*
*   Title Screen Functions Definitions (Init, Update, Draw, Unload)
*
*   Copyright (c) 2014-2022 Ramon Santamaria (@raysan5)
*
*   This software is provided "as-is", without any express or implied warranty. In no event
*   will the authors be held liable for any damages arising from the use of this software.
*
*   Permission is granted to anyone to use this software for any purpose, including commercial
*   applications, and to alter it and redistribute it freely, subject to the following restrictions:
*
*     1. The origin of this software must not be misrepresented; you must not claim that you
*     wrote the original software. If you use this software in a product, an acknowledgment
*     in the product documentation would be appreciated but is not required.
*
*     2. Altered source versions must be plainly marked as such, and must not be misrepresented
*     as being the original software.
*
*     3. This notice may not be removed or altered from any source distribution.
*
**********************************************************************************************/

#include "raylib.h"
#include "screens.h"

//----------------------------------------------------------------------------------
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen = 0;
static Timer blink = {0};

//----------------------------------------------------------------------------------
// Title Screen Functions Definition
//----------------------------------------------------------------------------------

// Title Screen Initialization logic
void InitTitleScreen(void)
{
    // TODO: Initialize TITLE screen variables here!
    framesCounter = 0;
    finishScreen = 0;
    blink = (Timer){};
    StartTimer(&blink, 1.0);
}

// Title Screen Update logic
void UpdateTitleScreen(void)
{
    // TODO: Update TITLE screen variables here!

    // Press enter or tap to change to GAMEPLAY screen
    if ( IsGestureDetected(GESTURE_TAP) || IsMouseButtonPressed(MOUSE_BUTTON_LEFT)){
        //finishScreen = 1;   // OPTIONS
        finishScreen = 2;   // GAMEPLAY
        PlaySoundMulti(soundEffects[CLICK]);
    }
}

// Title Screen Draw logic
void DrawTitleScreen(void)
{
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), BLACK);
    DrawTextEx(font, "-Jumpex-", (Vector2){ screenWidth*5/14, 10 }, font.baseSize*4, 5, WHITE);
    float elasped = GetElapsed(&blink);
    if( elasped <= 0.5 ){
        DrawText("Tap to play !", screenWidth*5/14, 220, font.baseSize*2, WHITE);
    } else if ( elasped > 0.5 ) {
        if (TimerDone(&blink)) StartTimer(&blink, 1.0);
    }
    DrawText(TextFormat("Best Score : %i", bestScore), screenWidth*5/14-20, 100, font.baseSize*2, WHITE);
    
    DrawText(" Published 29/01/22 ", 10, screenHeight-60, font.baseSize, RED);
    DrawText(" Dub Music : FootPrint System - Subversion", 10, screenHeight-40, font.baseSize, YELLOW);
    DrawText(" By Quentin RAYMONDAUD - https://gitlab.com/Raymondaud.Q/jumpex", 10, screenHeight-20, font.baseSize, GREEN);

}

// Title Screen Unload logic
void UnloadTitleScreen(void)
{
    // TODO: Unload TITLE screen variables here!
}

// Title Screen should finish?
int FinishTitleScreen(void)
{
    return finishScreen;
}